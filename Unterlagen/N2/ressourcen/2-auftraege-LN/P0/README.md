# Subnetting: 6 Übungen im vierten Oktett

### Arbeitsauftrag:
- Hands-on Auftrag mit Filius. In diesem Auftrag verschieben wir die Subnetzmaske in das letzte Oktett der IP-Adresse. Ziel ist es, herauszufinden, welche Einflüsse solche Änderungen der Subnetzmaske auf bestehende Geräte im Netzwerk haben kann.


### Vorgehen:

**1. Auftrag und gezippte Filiusfiles downloaden**

- Downloade den vorbereiteten  [Arbeitsauftrag](m117_subnetting.pdf)
- Downloade die gezippten [Filiusfiles](m117-Filius-Files.zip)
- Öffne das "runtergeladene" File mit einem PDF-Reader (Inline-Felder zum ausfüllen)

**2. Auftrag durchführen**
 - Arbeitsauftrag durchlesen. **Achtung!** Auf der dritten Seite wird anhand des ersten Beispiels gezeigt, wie die Felder ausgefüllt werden sollen. 
 - Dazu das erste Filiusfile (1-subnet.fls) öffnen. 
 - Anschliessend die folgenden Aufgaben genau gleich lösen. Zu jeder Aufgabe gibt es ein entsprechendes Filius-file (6 Stk. im Zip enthalten)

**3. Auftrag abgeben**
 - PDF-File als `Nachname-Subnetting.pdf` gemäss Angaben der Lehrperson abgeben



<br>


---

> [⇧ **Zurück zu N2**](../../../README.md)

---


> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---