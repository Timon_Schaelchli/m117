# Benutzer, Gruppen, Berechtigungen und Freigaben einrichten

Bei praktisch jeder Installation einer Arbeitsstation werden sie nach einem
**Benutzernamen**, einem **Computernamen** und einem **Gruppennamen** gefragt.
Diese Angaben dienen einerseits zur Zugehörigkeit (Arbeitsgruppe) im Netzwerk,
anderseits zur klaren Identifizierung des Gerätes (Computername, Host) im
Netzwerk.

Je nach Arbeitsweise muss die Zusammenarbeit in der Arbeitsgruppe und mit den
einzelnen Computern stufengerecht geregelt werden.

## Einführung

### Ein Benutzer und ein Computer
Die einfachste Arbeitsweise ist,
wenn **ein Benutzer** mit einem Computer arbeitet. Aus Sicherheitsgründen muss
ein Passwortschutz gesetzt werden. Da der ganze Computer einem Eigentümer
gehört, sind aber **keine Berechtigungen** innerhalb des Systems nötig.

![1user1compi](images/10_200px.png)

### Ein Benutzer verwendet mehrere Computer
Auch hier spielt die **Berechtigung** auf den einzelnen Computer noch **keine Rolle**. Dafür hat man eher Probleme mit der Organisation der Daten, sowie mit dem Aussehen des Desktops. Kein Computer wird das gleiche Erscheinungsbild aufweisen.

![1userXcompis](images/11_200px.png)

### Mehrere Benutzer arbeiten auf dem gleichen Computer

Diese Arbeitsweise **bedingt**
eine Zuweisung der **Berechtigung** auf verschiedene Verzeichnisse.   
Einige Verzeichnisse mögen persönlich sein, andere gemeinsam genutzt werden.

![XuserXcompis](images/12_300px.png)

### Verschiedene Benutzer arbeiten in unterschiedlichen Gruppen

Benutzer werden in der Regel in **Gruppen** organisiert. Die Gruppen bilden meist **organisatorische Einheiten** oder **Geschäftsbereiche** innerhalb der Firma ab, wie z.B. Abteilungen (Marketing, Verkauf, Entwicklung, usw.) oder auch **operative Einheiten** wie z.B. Druckerverwaltung, Benutzermanager, Ressourcen, usw.   Benutzer **können** Mitglieder von **mehreren Gruppen** sein.

![XuserXgruppen](images/13_400px.png)

## Komplexe Strukturen, Benutzer und Computer

In einem Netzwerk entstehen rasch
**komplexe Strukturen**. Die Zugriffe auf Verzeichnisse und andere Ressourcen (Drucker, Internet, usw.) unterstehen vielfältigen betrieblichen Regelungen.

In einem solchen Umfeld muss die Zusammenarbeit zwingend über **Berechtigungen geregelt** werden

![kompl-struktur](images/14_500px.png)



## Berechtigungen

Im Microsoft-Umfeld, bei Windows werden **drei verschiedene Berechtigungen**
verwendet:

**Laufwerkzugriff** (2x)

![berechtigungen1](images/15_Tabelle1_800.png)

**Netzwerkzugriff** (1x)

![berechtigungen2](images/15_Tabelle2_800.png)

**Grafik "Berechtigungen" und "Netzwerk-Freigabe"**

![grafik](images/16_Grafik_400.png)

### Eine Türe ist nicht ein Stuhl....

Der Unterschied zwischen der Berechtigung auf Freigabeebene und der Berechtigung
auf Verzeichnisebene ist oft verwirrend...
Die Berechtigung auf **Freigabeebene** ist mit einer **Zimmertüre** zu vergleichen. Alle die durch diese Tür in einen Raum wollen, müssen die Berechtigung dazu haben.

Ist man **im Raum** gelten dort weitere Berechtigungen (**die Datei- und
Verzeichnisberechtigungen**) um z.B. auf einen Stuhl sitzen zu dürfen oder den
Kühlschrank zu öffnen.

Anmerkung: Microsoft sieht standardmässig vor, dass die Türe **nicht** verschlossen ist (nur Lesezugriff).

## Vererbung von Berechtigungen

Um das Verteilung von Berechtigungen zu vereinfachen, werden eingestellte Rechte
**immer an sämtliche Dateien und Unterordner eines Ordners weitergegeben** - also **vererbt**.  
Die Vererbung kann jederzeit unterbrochen resp. geändert werden, ab dort gelten
dann die neuen Rechte.

Beispiel:
(S: schreiben, L: lesen)


![grafik](images/17_Grafik2_600.png)

<br>

# Ergänzungen

## Unterschied Gruppe \<-\> Arbeitsgruppe

Mit **Gruppe** oder Benutzergruppe sind **mehrere Benutzer** gemeint, die die
gleichen Rechte haben. Die Rechte einer solchen Gruppe von Benutzern können entweder im ganzen Netz zentral verwaltet werden oder auf jedem Computer einzeln.

Unter **Arbeitsgruppe** versteht man *bei Windows-Betriebssystemen* eine
**Gruppe von Computern**, die sich gegenseitig Ressourcen wie Verzeichnis oder
Drucker zur Verfügung stellen können.

## Unterschied Windows-Arbeitsgruppe \<-\> Windows-Domäne

Wird die Regelung der Rechte auf jedem Computer einzeln gelöst, spricht man *bei
Windows* von einer Arbeitsgruppe. In einer Arbeitsgruppe muss jeder Computer
seine eigene Richtlinie verwalten. Diese Richtlinien müssen auch auf jedem
Computer konfiguriert werden!

Führt man die Regelung der Rechte für ein lokales Netzwerk zentral an einem Ort
aus, spricht man *unter Windows* von einer Domäne (nicht zu verwechseln mit
einer DNS-Domäne, einem Namensbereich auf dem Internet) oder von einem
Verzeichnisdienst. Im Modul 117 konzentrieren wir uns auf die Arbeitsgruppe.

![grafik3](images/21_Grafik_600.png)

<br><br>

# Praxisteil
## Warm-up für LB2-Projektarbeit

Wir werden nun beispielhaft Schritte durchführen, die bei der **LB2 Praxisarbeit, Sota GmbH** genutzt und weiterentwickelt werden können.

## Einrichten einer Arbeitsgruppen mit Freigaben und Berechtigungen für Benutzer und Gruppen

Damit die Sota GmbH die **Vorteile des neuen Netzwerkes** nutzen kann, müssen wir
eine gemeinsame Arbeitsgruppe mit Verzeichnissen, die für andere Benützer über das Netz freigegeben sind, einrichten.

In einem Peer-to-Peer-Netzwerk muss man die Berechtigungen der einzelnen
Mitarbeiter jedoch genauer unter die Lupe nehmen. Ansonsten könnten **alle Benutzer von jedem Computer** auch alle Daten einsehen und willentlich, oder unwissentlich Daten auf anderen Computern löschen oder verändern. Um diese Rechte auf das Nötige zu beschränken werden wir den einzelnen Benutzern und (Benutzer-)Gruppen unterschiedliche Berechtigungen zuweisen.

## Vorgehen

- Zuerst wird eine Planung mit den einzelnen Teilzielen und den zugehörigen
Terminen definiert.

- Danach werden die nötigen Vorarbeiten **Elektronisch oder auf Papier (!)** geleistet.   
Definition des Konzepts *(Mitarbeiterkürzel gem. Konvention,Benutzer, Logins, Gruppen, Verzeichnisse* und *Rechte*)

- **Erst dann (!)** wird gemäss diesem (jetzt vorhandenen) Konzept alles umgesetzt und konfiguriert

- Beim **LB2-Projekt** wird dieses Konzept mit dem
![Dritten Teilauftrag](../../Leistungsbeurteilungen/LB2/03_IT-Konzept_+_Systemdoku_v1.2.pdf) umgesetzt

- Zuletzt werden die Teilziele mit den vorher definierten Testbedingungen
**überprüft**.

## Ablauf

Der Ablauf entspricht den oben aufgeführten Punkten:

- Erstellen Sie eine Planung mit Hilfe der hinten angefügten Ihnen bekannten
Checkliste und definieren Sie darin zu jedem Schritt das Testvorgehen. Lesen Sie
dazu die **folgenden Abschnitte** durch.

- Die **Definition** von **Mitarbeiterkürzel** (Login), **Passwörter**, **Gruppen**, **Verzeichnisse** und **Rechte** finden auch in den folgenden Abschnitten statt.

- **Konfigurieren** Sie für die Sota GmbH die Benutzer, Gruppen Verzeichnisse
und Verzeichnis-Berechtigungen wie Sie in Abschnitt „Verzeichnis-Berechtigung
von Gruppen und Benutzern definieren“ festgelegt sind.   
Geben Sie alle Verzeichnisse einzeln frei (Freigaben).

- Testen Sie nach ihren Testbedingungen

## Benutzer und Benutzernamen definieren

Eine Berechtigung ist immer mit **einer Person**, oder **einer Gruppe**
verbunden. Aus diesem Grund müssen zuerst die **Mitarbeiter** erfasst werden. Bei der Sota GmbH sind es zurzeit 8 Mitarbeiter.

Eine geeignete Form um bei der Anmeldung nicht den ganzen Namen zu verwenden,
ist die Verwendung des **Kurzzeichens** als sogenannter **Login**.   
Bei der Sota GmbH hat man für das Kurzzeichen, die **ersten drei Buchstaben des Nachnamen** und die **ersten drei Buchstaben des Vornamen** gewählt (NNNVVV). Der neu eintretende Mitarbeiter **Luca Graf** würde das Kurzzeichen **GRALUC** erhalten.

| **Mitarbeiter** | **Kurzzeichen (Login)** |
|-----------------|-----|
| Lorenz Fischer     | FISLOR |
| Luca Dali          | DALLUC |
| ......             | XXXXXX |


## Gruppen definieren

Gruppen werden gebildet um **Berechtigungen auf Gruppenebene**, statt auf Personenebene zu verteilen. Durch das Zuordnen einer Person zu einer Gruppe, gelten für die Person automatisch die Rechte der Gruppe.
**Dadurch wird die Vergabe von Berechtigungen wesentlich einfacher**.   
Eine Gruppe kann eine Abteilung, eine Projektgruppe oder eine Anwendung sein.

Sinnvollerweise betrachtet man zuerst die Funktionen der Mitarbeiter. Wie aus der nächsten Tabelle zu entnehmen ist, können aus den Funktionen einige Gruppen zusammengestellt werden.

| **Mitarbeiter** | **Funktion** | **Gruppe** |
|---|---|---|
| Derungs Eva | CEO, Kundenberater, Personal | Alle |
| Fischer Lorenz  | Kundenberater, Sekretariat | Verkauf |
| Dali Luca    | Logistiker  | Abwicklung  |
| ...    | ...  | ...  |

## Dateiablage definieren

Bevor wir die eigentliche Umsetzung auf den Computer machen, müssen wir uns Gedanken über die Dateiablage machen. Erst wenn wir wissen, wo welche Daten
abgelegt sind, können wir die Berechtigungen verteilen.

In der Praxis wird oft die Unterteilung in **allgemeine Daten**, **persönliche
Daten** und **Gruppendaten** gemacht. Diese einfache Unterteilung hilft uns zu
verstehen, wie und warum man die Berechtigungen vergeben muss.
Selbstverständlich kann diese Unterteilung weiter verfeinert und der jeweiligen Situation angepasst werden.

| **Allgemeine Daten** | **Persönliche Daten** | **Gruppendaten** |
|---|---|---|
| Vorlagen | Entwürfe | Einkauf |
| Informationen  | Email | Verkauf |
| Berichte | Pendenzen | Abwicklung  |
| ...    | ...  | ...  |


Die Daten müssen in entsprechende Verzeichnisse (Ordner) auf den einzelnen
Computern verteilt werden. Die **Gruppendaten** werden nach Funktion der
Mitarbeiter verteilt. Der Einkäufer verwaltet den Ordner Einkauf, der Verkäufer, den Ordner Verkauf, usw.

Die **allgemeinen Daten** werden am besten von einem Sachbearbeiter des
Sekretariats verwaltet. In einer kleinen Firma kann es die Person sein, die am
häufigsten im Büro ist, in einer grösseren Firma ist es das Sekretariat einer Abteilung.

Die **persönlichen Daten** sind auf dem Computer, den der Benutzer verwendet. Auch
wenn es mehrere Mitarbeiter auf dem gleichen Computer sind (z.B.
Teilzeitmitarbeiter), werden die persönlichen Daten in einem eigenen
Benutzerverzeichnis geschützt abgelegt. Diese Verzeichnisse werden vom
Betriebssystem oft automatisch erstellt und verwaltet, der Speicherort kann bei
Bedarf aber angepasst werden.

**Für die Sota GmbH** wird folgende Dateiablage definiert:

| **Verzeichnis** | **Funktion**  |
|---|---|
| C:\\Sota\\**Verkauf**     | Gruppendaten Verkauf     |
| C:\\Sota\\**Finanzen**     | Gruppendaten Finanzen     |
| C:\\Sota\\**Abwicklung** | Gruppendaten Abwicklung |


**Achtung:** Die speziellen Verzeichnisse befinden sich unterhalb **C:\\Sota\\**

## Verzeichnis-Berechtigung für Gruppen und Benutzern definieren

Definieren wir jetzt die Berechtigung auf die einzelnen Verzeichnisse für den
Harddiskzugriff. Dazu brauchen wir zuerst die Namen und die Funktionen der
einzelnen Mitarbeiter.

**Hinweis:** Berechtigungen werden in erster Linie **nur Gruppen** erteilt und
**nicht (!)** einzelnen Benutzern. Damit erspart man sich u.a. Administrationsaufwand bei neuen Benutzern. Diese werden nur den entsprechenden Gruppen zugeteilt und erhalten damit automatisch die richtigen Berechtigungen.

Die Art der Berechtigung unterteilen wir in **drei Kategorien**:

-   **(L) Lesen**: Der Benutzer kann eine Datei zum Lesen öffnen

-   **(S) Schreiben**: Der Benutzer kann zusätzlich zu „L“ eine Datei verändern,
    speichern, löschen

-   **(-) Kein Zugriff**: Der Benutzer hat keinen Zugriff auf die Datei

Die Definitionen werden in einer **Berechtigungsmatrix** festgehalten:

| **Gruppe** | Mitglied  |  |  |  |
|---|---|---|---|---|
| |  | **Verkauf** | **Finanzen** | **Abwicklung** |
| **Verkauf**  | KEHURS, FISLOR, MEIANT  | **S**  | **L**  | **L**  |
| **Finanzen**  | XXXXXX, XXXXXX  | **?**  | **?**  | **?**  |
| **Abwicklung**  | XXXXXX  | **?**  | **?**  | **?**  |
| **GL**  | XXXXXX  | **?**  | **?**  | **?**  |


<br>

## Benutzer einrichten

In diesem Abschnitt wird die Vorgehensweise bei der Verwaltung mit Windows beschrieben (Linux folgt noch)

**Hinweis:** Seit Windows XP unterscheidet Windows zwischen der **einfachen** und der **erweiterten Benutzerverwaltung**.
Die **einfache Benutzerverwaltung** ist erreichbar über
> *Systemsteuerung -\> Benutzerkonten* 

Diese Verwaltung reicht für unsere Zwecke aber **nicht**.

Die **erweitere Benutzerverwaltung** erreicht man über 
>*Computerverwaltung \-\> Lokale Benutzer und Gruppen*

Die Computerverwaltung ist über folgende zwei Wege erreichbar:
>*Systemsteuerung -\> Verwaltung -\> Computerverwaltung* 

>*Rechtsklick auf
Computer-Symbol -\> Verwaltung*

Benutzer und Gruppen | 
---| 
![benutzerverwaltung](images/24_Benutzerverwalt_800.png)


 Aktion / Neuer Benutzer | Vorgehen
| :---: | :---|
![Benutzer](images/25_Benutzer.png)  | **Benutzer anwählen**: Über "**Aktion / Neuer Benutzer**“ (oder rechte Maustaste und Kontextmenu) kann der neue Benutzer erfasst werden. Geben Sie seinen Benutzernamen, Zusatzinfos und ein Passwort ein. **Achtung!** Das Startpasswort muss dem Benutzer mitgeteilt werden und sollte bei der ersten Anmeldung aus Sicherheitsgründen geändert werden. Dieser Vorgang kann für die Erstanmeldung erzwungen werden

<br>

## Gruppe einrichten

Jeder Computer hat systembedingte (vordefinierte) Gruppen. Dazu gehören u.a.
Administratoren, Benutzer, Operatoren, usw. Diese Gruppen werden für die Verwaltung der Computer verwendet.

![gruppe](images/26_Gruppen_800.png)


 Aktion / Neue Gruppe | Vorgehen
| :---: | :---|
![Benutzer](images/27_user2group.png)  | **Gruppen anwählen**: Über "**Aktion / Neue Gruppe**" (oder rechte Maustaste und Kontextmenu) kann die neue Gruppe erfasst werden: Der Gruppe können über *"Hinzufügen"* die zugehörigen Benutzer zugeteielt werden. Erstellen Sie **alle** aufgeführten **Mitarbeiter** und **Gruppen** der Sota GmbH und **teilen Sie die Benutzer den Gruppen zu**.


**Add-on**

Zur Erstellung von Benutzern, Gruppen und Verzeichnisberechtigungen habe ich folgendes **6Min.-Tutorial** erstellt:

**Configuring Users, Groups and Folder Permissions on Windows 10** (on a VMware-Workstation VM)

![Clip:](../../x_gitressourcen/Video.png) 6min
[![Tutorial](../../Leistungsbeurteilungen/LB2/images/01_Lab_Setup_part7_200.png)](https://web.microsoftstream.com/video/64ac2a01-a072-4de6-ad1d-14761ca9aef5)



<br>

## Verzeichnisse für die Dateiablage einrichten

Sinnvollerweise wird ein **Hauptverzeichnis** (im Fall der Firma Sota GmbH **"Sota"**) erstellt. Dieses Verzeichnis wird später auch für das Backup verwendet. Darin werden die entsprechenden
**Unterverzeichnisse** aufgebaut.  
(Wahlweise werden Daten auch in einer eigenen Partition (Laufwerk) anstelle
eines gemeinsamen Verzeichnisses abgelegt)

Um die verschiedenen Verzeichnisse und deren Berechtigung auszutesten, müssen
alle Verzeichnisse auf dem lokalen Computer eingerichtet werden.

**Hinweis:** Für die Sota GmbH erstellen wir (übungshalber) **nur auf einem Computer** (auf einer virtuellen Maschinen) **alle** diese Verzeichnisse. Die Benutzer der anderen Computer (zweite VM) sollen dann über das Netzwerk auf diese Verzeichnisse zugreifen können. Das Topverzeichnis (SOTA) wird für alle Benutzer freigegeben. Die Unterverzeichnisse werden dann entsprechend der Berechtigungsmatrix (Abteilungen) berechtigt.
In der Praxis ist dies **nicht sinnvoll**, es können sich daraus folgende Probleme ergeben:

-   Keine Ordnung (wo sind die Daten?)
-   Datensicherung aufwändiger
-   Administrierung aufwändiger
-   Gefahr redundanter (doppelter) Daten

Wir werden in den Folgemodulen **Serverdienste** kennenlernen, welche diese Aufgabe einiges eleganter und effektiver lösen. In diesem Modul geht es in erster Linie darum, das Konzept der Berechtigungszuweisung (User werden Gruppen zugeordnet - nur Gruppenberechtigungen werden vergeben) kennenzulernen und zu verstehen. Später lernen wir auch noch die Berechtigung von Rollen (Role Based Access Control) kennen. In diesem Konzept werden bestimmten Benutzern Rollen mit gezielt zugeschhnittenen Berechtigungen zugewiesen. 

Erstellen Sie die folgenden Abteilungs- erzeichnisse im Hauptverzeichnis „**C:\\\Sota**“:

| **Verkauf** | **Finanzen** | **Logistik** | **GL** "|
|---|---|---|---|
Dokumente der Abteilung "Verkauf" | Dokumente der Abteilung "Finanzen" | Dokumente der Abteilung "Logistik" | Dokumente der "GL"


**Umsetzung auf dem System** (VM)

Verzeichnis **Sota** unter **C:** erstellen und darin die oben definierten Unterverzeichnisse hinzufügen (Achten Sie auf den Text im Bild unten - es folgen weitere)

![Ordner](images/28_ordnerberecht_600.png)



## Verzeichnis–Berechtigung einrichten (Sicherheit)

Die Verzeichnis–Berechtigung (oder Datei-Berechtigung) ist das **meist
eingesetzte Mittel**, um eine einfache und klare Zuweisung für Berechtigung und
Benutzer zu erteilen.  
Vorgehen: Eigenschaften vom freigegebenen Ordner -\> **Sicherheit**

![Berechtigungen](images/32_Berechtigungen_600.png)

**Achtung:** Das Dateisystem muss vom Typ **NTFS** sein. Beim **FAT32**
Dateisystem **fehlt das Register Sicherheit**.  
Bei FAT32 können nur Berechtigungen auf Freigabeebene, d.h. für Zugriffe aus dem
Netzwerk definiert werden. Für den lokalen Zugriff kann **kein Schutz** definiert werden.

**Hinweis:** Externe Datenträger wie USB-Sticks oder USB-Festplatten, sind oft mit FAT32 formatiert. Bei Bedarf kann das Filesystem einfach auf NTFS geändert werden mit dem Konsolenbefehl **convert**.   
Das funktioniert ohne Datenverlust.

Beispiel vom Ordner **Finanzen**: 
Bevor die Berechtigungen verändert oder angepasst werden können, **muss** die **Vererbung deaktiviert** werden (siehe Bild unten oder das oben verlinkte Tutorial)

Eigenschaften des Ordners --> Erweitert |  Erweiterte Sicherheitseinstellungen --> Vererbung deaktivieren
:---:|:---:
[![star](images/33_Berechtigungen_800.png)](images/33_Berechtigungen_800.png)    |  [![switch5](images/34_Berechtigungen_800.png)](images/34_Berechtigungen_800.png)

Sobald die **Vererbung** deaktiviert ist, können die Berechtigungen geändert werden.

Über **Bearbeiten** können die Berechtigungen gesetzt resp. geändert werden.  
Für jede Gruppe oder jeden Benutzer wird individuell die Berechtigung gemäss
Berechtigungsmatrix eingestellt.  
Die gewünschten Gruppen oder Benutzer werden über **Hinzufügen** hinzugefügt.

Hinweis: Für den Ordner gelten die Berechtigungen, die in diesem Fenster
sichtbar sind. Es sollten darum **nur die Berechtigungen** vorhanden sein, die
gemäss Berechtigungsmatrix definiert sind, damit nicht unerwünschte
Berechtigungen vorhanden sind.   
Vererbte Berechtigungen (z.B. von ‚Benutzer‘) müssen darum i.d.R. entfernt resp.
unterbrochen werden.   
Siehe dazu auch unter ‚Erweitert‘ -\> „Berechtigungen ändern -\> „Vererbbare
Berechtigungen des übergeordneten Objektes…“.

![Berechtigungen](images/35_Berechtigungen_verk_800.png)

Das folgende Bild zeigt die Abteilungsberechtigungen Verkauf und Finanzen für den **Ordner Finanzen** 

Verkauf |  Finanzen
:---:|:---:
[![Verkauf](images/36_Berechtigungen_800.png)](images/36_Berechtigungen_800.png)    |  [![Finanzen](images/37_Berechtigungen_800.png)](images/37_Berechtigungen_800.png)

### Aufgabe

Richten Sie die Verzeichnisrechte nach der definierten **Berechtigungsmatrix**
ein.   
Vergessen Sie nicht, die gesetzten Berechtigungen auch zu überprüfen!

Wir haben in der Berechtigungsmatrix die Berechtigungen nur allgemein mit **(S) Schreiben**, **(L) Lesen** und **(-) Kein Zugriff** definiert. Dies müssen wir auf die unter Windows verfügbaren Berechtigungen abbilden.

Welche Felder müssen markiert sein für unser allgemeines **Schreiben (S)**

![Berechtigungen](images/38_Voll_300.png)

Welche Felder müssen markiert sein für unser allgemeines **Lesen (L)**

![Berechtigungen](images/38_Lesen_300.png)

Welche Felder müssen markiert sein für unser allgemeines **Kein Zugriff (-)**

![Berechtigungen](images/38_Nix_300.png)



### Und noch das …

Die Berechtigung **"Vollzugriff"** sollte in der Regel **nur Administratoren** zugeteilt werden. Der Vollzugriff erlaubt u.A. das Ändern der Berechtigungen.

Ein Benutzer kann in mehr als einer Gruppe Mitglied sein. Damit erhält er auch die Rechte aller Gruppen. Die Rechte **addieren** sich dabei.

Die Rechte unter **"Verweigern"** **haben immer Priorität** **(!)**, d.h. wenn ein Recht einmal verweigert wird, bleibt es verweigert, auch wenn es in einer anderen Gruppenberechtigung erteilt ist.  
Die Verweigern-Rechte sind normalerweise **nicht notwendig** und können zu einem Durcheinander führen.  
Verwenden Sie darum **keine Verweigern-Rechte!**


<br>

## Verzeichnisse freigeben und Freigabe–Berechtigung einrichten

Um ein Verzeichnis **für den Netzwerkzugriff freizugeben** *(d.h. das
Verzeichnis wird im Netzwerk sichtbar)* gehen Sie wie folgt vor:   
Eigenschaften des Verzeichnisses -\> Freigaben -\> **Erweiterte Freigabe** -\>
Diesen Ordner freigeben  
Der Freigabename ist frei wählbar, vorgeschlagen wird der Name des freigegebenen
Verzeichnisses.

Achtung: Der Knopf „Freigabe“ ist für uns untauglich, darum **„Erweiterte
Freigabe“**.

Unter *Berechtigungen (auf Freigabeebene)*, d.h. die Rechte für den Netzwerkzugriff, können bei jedem freigegebenen Verzeichnis für einen
zusätzlichen Schutz eingestellt werden (neben der Verzeichnisberechtigung für
den Harddiskzugriff.  
Normalerweise genügt die *Berechtigung auf Verzeichnisebene*. In der Praxis wird darum die *Berechtigung auf Freigabeebene* **nicht (!)** eingeschränkt: Darum
**Gruppe „Jeder“** auf **Vollzugriff***.  
*Achtung: Die die *Berechtigung auf der Verzeichnisebene* werden anschliessend
noch vergeben!

![Freigabe](images/29_Freigabe_LATEST_800.png)

Geben Sie den **übergeordneten** Ordner **SOTA** frei.

Dieses freigegebene Verzeichnis ist jetzt für **alle** Computer im gleichen Netzwerk sichtbar.

Zur Kontrolle können sie über **Netzwerk** auf ihren eigenen Computer zugreifen.
Damit schauen Sie *wie über das Netzwerk* auf Ihren eigenen Computer.   
Das ist dieselbe Sicht, die jemand hat, wenn er über das Netzwerk auf Ihren
Computer zugreift.

**Hinweis:** Die Netzwerkumgebung zeigt nicht sehr zuverlässig die Freigaben an. Sie können darum auch im Windows-Explorer in der Adresszeile den **Computernamen** oder (sicherer) **die IP-Adresse** eingeben mit vorlaufenden zwei Backslashes
„\\\\“, z.B. „\\\\PC-07“ oder „\\\\192.168.100.57“. Die Freigaben des Computers werden angezeigt.

![Freigabe](images/30_Remotezugriff_800.png)

Kontrolle, ob die Freigabe **sichtbar** ist.
Über **VMware DNS** und über **IP-Adresse**

![Freigabe](images/31_Remotezugriff_800.png)


**Add-on**

Zur Konfiguration von Verzeichnis- und Netzwerkfreigaben habe ich folgendes **6Min.-Tutorial** erstellt:


**Configuring a Directory-share for users and groups on the same Network** (on a VMware-Workstation VM)

![Clip:](../../x_gitressourcen/Video.png) 6min
[![Tutorial](../../Leistungsbeurteilungen/LB2/images/01_Lab_Setup_part8_200.png)](https://web.microsoftstream.com/video/82ec4a69-3328-499c-af44-c54df3c2b83e)

<br>


---

> [⇧ **Zurück zu N2**](README.md)

---

> [⇧ **Zurück zu Unterlagen**](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---

