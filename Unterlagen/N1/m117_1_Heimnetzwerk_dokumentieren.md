# Heimnetzwerk kennenlernen und dokumentieren

## Aufgabenstellung
Mit diesem ersten Auftrag untersuchen sie ihr Heimnetzwerk. Je nach Wissensstand und den Möglichkeiten zu Hause, dürfen sie die unten aufgelisteten Punkte mehr oder weniger ausführlich recherchieren und festhalten. 

**Wichtig:** Bitte **keine** Konfigurations-Veränderungen vornehmen.

**1. Teilauftrag** Handskizze (zu Hause recherchieren und festhalten)

1.  Dokumentieren und skizzieren Sie Ihr Heimnetzwerk als Netzwerkplan von Layer 1-4 mit allen vorhandenen Geräten inklusive der vorhandenen Schnittstellen.

    -   Layer 1 Netzwerkkarte und Übertragungsmedium

    -   Layer 2 Switch (Anzahl Ports) wenn vorhanden und MAC-Adressen der Netzwerkkarten

    -   Layer 3 Router (IP-Adresse im internen Netz, IP-Adresse vom Provider)

    -   Layer 4 Default-Gateway (Einstellung auf Client / Router)


2.  Dokumentieren Sie die Anbindung an das Internet (Provider, Übertragungsmedium, Verbindungsgeschwindigkeit). Was steht im Vertrag,
    welche Geschwindigkeit haben Sie wirklich? Messung mit [www.cnlab.ch](http://www.cnlab.ch/), Auswertung dokumentieren

3.  Ordnen Sie die dokumentierten Angaben jeweils den OSI-Layern zu (z.B. durch farbliche Unterscheidung).

<br>

---

<br>

**2. Teilauftrag** Dokumentation Heimnetzwerk

1.  Überlegen Sie sich, welche Inhalte am besten in die Dokumentation passen. Was macht Sinn, was nicht?

2.  Entscheiden Sie sich für Textverarbeitungssoftware (z.B. MS Word, Google Docs, LibreOffice Writer). Erstellen Sie eine Doku-Vorlage mit einem sinnvollen Inhaltsverzeichnis (Frei wählbar).

3.  Dokumentieren sie die erhobenen Daten nun und bringen sie diese in eine möglichst sinnvolle Struktur. Für die Umsetzung des Netzwerkplans verwenden Sie entweder Microsoft Visio (kann auf dem [Azure Portal](https://portal.azure.com/#home) bezogen werden) oder [draw.io](https://www.diagrams.net/) (Freeware).

