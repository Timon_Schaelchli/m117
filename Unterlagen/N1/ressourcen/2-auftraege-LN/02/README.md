# Filius Kleinbetrieb

### Arbeitsauftrag:
- Hands-on Auftrag mit Filius. 


### Vorgehen:

**1. Auftrag downloaden**

- Lade den vorbereiteten  [Arbeitsauftrag](m117_Filius_Kleinbetrieb.pdf) runter

- Öffne das "runtergeladene" File mit einem PDF-Reader

**2. Auftrag durchführen**
 - Arbeitsauftrag durchlesen
 - Textbearbeitungsprogramm (Markdown, Word) öffnen und strukturieren
 - Filius öffnen
 - Gemäss Kapitel **Vorgehensweise** durchführen

<br>

---

> [⇧ **Zurück zu N1**](../../../README.md)

---


> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---