# M117 - Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren

Dieses Repository enthält Ressourcen für die Lernenden zur Durchführung des Moduls M117 an der TBZ. Es basiert auf den Vorgaben der BIVO2021, bzw. den vorbereitenden Arbeiten von [ModulentwicklungZH M117](https://gitlab.com/modulentwicklungzh/cluster-platform/m117). 

Zugehörendes [Repository M117 für die Lehrpersonen](https://gitlab.com/ch-tbz-it/TE/m117) auf GitLab.


### Hier geht es zu den Inhalten

- [**Hauptscript**](Ressourcen/M117-V4.pdf) (Autor: Johann Widmer, dieses Dokument wird nicht weiterentwickelt und Ende Schuljahr 23/24 entfernt)

- [**Unterlagen:** Hauptverzeichnis](Unterlagen)
---
- [**N1:** Basic Skills](Unterlagen/N1) 
- [**N2:** Advanced Skills](Unterlagen/N2)
- [**N3:** Expert Skills](Unterlagen/N3)
---
- [**LB2** Projektarbeit](Leistungsbeurteilungen/LB2)

<br>