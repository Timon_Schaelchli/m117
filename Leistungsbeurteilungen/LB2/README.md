## LB2 Projektarbeit 
__(Sozialform vorzugsweise Tandem)__

Bei dieser **Leistungsbeurteilung** handelt es sich um ein SOL-Projekt mit **vier Teilaufträgen**. Um den Lernenden genügend Zeit zur Verfügung zu stellen, sollten ca. **20 Lektionen** dafür eingerechnet werden. Jeder Teilauftrag wird vorzugsweise separat abgegeben (Terminvereinbarung mit der Lehrperson).<br>
**Modulgewichtung: 50%** 

### Gliederung
Die Aufträge der **LB2** sind wie folgt gegliedert:

- [Gesamtauftrag](00_Auftrag_IT-Konzept_1.2.md)
- [Aufbau Physikalische und logische Netzwerktopologie](01_Physik+Log_Netzwerktopologie_v1.2.md) **(15% Gewichtung)**
- [Erstellen einer Offerte (inkl. Material und DL-Kosten)](02_Offerte_erstellen_v1.3.md) **(15% Gewichtung)**
- [Erstellen eines IT-Konzepts](03_IT-Konzept_+_Systemdoku_v1.2.pdf) **(15% Gewichtung)**
- [Installation + Konfiguration von zwei Arbeitsplätzen](04_Installation_+_Konfiguration_v1.2.md) **(55% Gewichtung)**

### Ressourcen
Hier sind Tutorials verlinkt, die bei der Umsetzung der **LB2** nützlich sein können

**Part 1: M117 LB2** - Gathering ISO-Images for your personal Lab-Environment

![Video1:](../../x_gitressourcen/Video.png) 6min
[![Tutorial](images/01_Lab_Setup_part1_200.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/marcello_calisto_tbz_ch/EUP3-RcBHnJPhMsNar6-zq8B1AcGviqpHvgfuafe7ix6oQ?e=4Wps4P&nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19) **Neuer Link - Stream Sharepoint**

![Video1:](../../x_gitressourcen/Video.png) 6min
[![Tutorial](images/01_Lab_Setup_part1_200.png)](https://web.microsoftstream.com/video/ce67627e-c9a1-44f9-9511-24108fdc227c) **Alter Link - Stream Classic** (wird anfangs 2024 entfernt)

---

<br>

**Part 2: M117 LB2** - Setting up a Virtual Machine with **VMware Workstation**

![Video2:](../../x_gitressourcen/Video.png) 8min
[![Tutorial](images/01_Lab_Setup_part2_200.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/marcello_calisto_tbz_ch/ESjjxu-TrQBNjK2q1ihcRQ4BiNPhJ6qg96LDfNkElivo7w?e=BN0wyl&nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19) **Neuer Link - Stream Sharepoint**

![Video2:](../../x_gitressourcen/Video.png) 8min
[![Tutorial](images/01_Lab_Setup_part2_200.png)](https://web.microsoftstream.com/video/e377066b-52dc-40e4-b9d8-2c81dc95d4e3) **Alter Link - Stream Classic** (wird anfangs 2024 entfernt)


---

<br>

**Part 3: M117 LB2** - Installing Windows 10 (on a VMware-Workstation VM)

![Video3:](../../x_gitressourcen/Video.png) 7min
[![Tutorial](images/01_Lab_Setup_part3_200.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/marcello_calisto_tbz_ch/EQBHfZc1_mhLiUKeHLKTie4BLo2M5OSmKCU4CUGhqjtHBg?e=aguu3u&nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19) **Neuer Link - Stream Sharepoint**

![Video3:](../../x_gitressourcen/Video.png) 7min
[![Tutorial](images/01_Lab_Setup_part3_200.png)](https://web.microsoftstream.com/video/17253824-62e3-4726-8edd-55e4d47aefd7)  **Alter Link - Stream Classic** (wird anfangs 2024 entfernt)

---

<br>

**Part 4: M117 LB2** - Installing VMware-Tools on Windows 10 (on a VMware-Workstation VM)

![Video4:](../../x_gitressourcen/Video.png) 2min
[![Tutorial](images/01_Lab_Setup_part4_200.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/marcello_calisto_tbz_ch/EdMPHM9xb2VKjFzz1uKgdgkBWuilU5R21D3kbenBm9aKXw?e=g9wqhZ&nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19)  **Neuer Link - Stream Sharepoint**

![Video4:](../../x_gitressourcen/Video.png) 2min
[![Tutorial](images/01_Lab_Setup_part4_200.png)](https://web.microsoftstream.com/video/33336895-8d02-4a18-8426-2921eff73ff0)   **Alter Link - Stream Classic** (wird anfangs 2024 entfernt)

---

<br>

**Part 5: M117 LB2** - Preparing Network and other Params on Windows 10 (on a VMware-Workstation VM)

![Video5:](../../x_gitressourcen/Video.png) 9min
[![Tutorial](images/01_Lab_Setup_part5_200.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/marcello_calisto_tbz_ch/EXxgbwHXubhHqcoYlvkq_y8BoOtx7z6kLkKbInoFzycGFw?e=dZnm1l&nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19)   **Neuer Link - Stream Sharepoint**

![Video5:](../../x_gitressourcen/Video.png) 9min
[![Tutorial](images/01_Lab_Setup_part5_200.png)](https://web.microsoftstream.com/video/fe5c705e-5b00-4f68-8e76-c5e6bad9884e)    **Alter Link - Stream Classic** (wird anfangs 2024 entfernt)

---

<br>

**Part 6: M117 LB2** - Configuring a private Network for 2 VMs (on a VMware-Workstation VM)

![Video6:](../../x_gitressourcen/Video.png) 9min
[![Tutorial](images/01_Lab_Setup_part6_200.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/marcello_calisto_tbz_ch/EePblOR6QvZNrmhxF1grDj0Br62wHgXEX7SAVi4REozhRA?e=qiaHer&nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19)  **Neuer Link - Stream Sharepoint**

![Video6:](../../x_gitressourcen/Video.png) 9min
[![Tutorial](images/01_Lab_Setup_part6_200.png)](https://web.microsoftstream.com/video/279f3ed6-4fd7-40ec-abcf-2c14cddf3a95) **Alter Link - Stream Classic** (wird anfangs 2024 entfernt)

---

<br>


**Part 7: M117 LB2** - Creating Users, Groups and granting access to specific folders and files (on a VMware-Workstation VM)

![Video7:](../../x_gitressourcen/Video.png) 6min
[![Tutorial](images/01_Lab_Setup_part7_200.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/marcello_calisto_tbz_ch/EdzO4eg-aS5IkSq79OgSP9cBCFqUKcyM9t137ZicajbKOg?e=9E7l5v&nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19) **Neuer Link - Stream Sharepoint**

![Video7:](../../x_gitressourcen/Video.png) 6min
[![Tutorial](images/01_Lab_Setup_part7_200.png)](https://web.microsoftstream.com/video/64ac2a01-a072-4de6-ad1d-14761ca9aef5) **Alter Link - Stream Classic** (wird anfangs 2024 entfernt)

---

<br>

**Part 8: M117 LB2** - Configuring a Directory-share for users and groups on the same Network (on a VMware-Workstation VM)

![Video8:](../../x_gitressourcen/Video.png) 7min
[![Tutorial](images/01_Lab_Setup_part8_200.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/marcello_calisto_tbz_ch/EaaWR-XESwpKkuJXMsx7210BjA2_3wuG2ncmtt9Hl2vRHg?e=CivXaC&nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19) **Neuer Link - Stream Sharepoint**

![Video8:](../../x_gitressourcen/Video.png) 7min
[![Tutorial](images/01_Lab_Setup_part8_200.png)](https://web.microsoftstream.com/video/82ec4a69-3328-499c-af44-c54df3c2b83e) **Alter Link - Stream Classic** (wird anfangs 2024 entfernt)

<br>




Bei Fragen, Anpassungen oder Wünschen, bitte bei mir melden

Modulverantwortung:
[Marcello Calisto](mailto://marcello.calisto@tbz.ch) 
