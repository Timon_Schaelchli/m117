# Auftrag IT-Konzept

## Lernziele: 
- Sie vertiefen die systematische Vorgehensmethode bei der Organisation von Projekten 
- Sie bauen organisatorische und technische Fachkompetenzen auf, um Projekte bei Kleinkunden korrekt zu planen und fachkundig zu realisieren

---

**Ausgangslage:**

Die Firma Sota GmbH ist in den vergangenen Jahren stark gewachsen. Aus diesem Grund sieht sich die Geschäftsleitung gezwungen, in ein anderes Gebäude mit mehr Bürofläche zu ziehen. Im neuen Büro soll eine komplett neue IT-Infrastruktur geplant und realisiert werden. Da Informatik nicht zum Kerngeschäft der Sota GmbH gehört, wird sie diesen Auftrag einer externen Firma vergeben. Dazu wurde bereits eine öffentliche Ausschreibung publiziert. 

Ihre Firma (Sie können ihr einen eigenen, fiktiven Namen geben) bietet genau solche IT-Lösungen an. Sie sind ein motiviertes Jungunternehmen und werden nun eine entsprechende Offerte ausarbeiten und der Geschäftsleitung der Firma Sota GmbH vorstellen **(1.+2. Teilauftrag)**. 
Die beste (nicht unbedingt die günstigste) Offerte erhält den Zuschlag. Geben Sie sich also Mühe und seien sie kreativ. 

Ihr Unternehmen hat die passendste Offerte abgegeben und den Zuschlag für die Durchführung erhalten. Jetzt geht es also darum, dieses Projekt erfolgreich und möglichst gewinnbringend für alle Stakeholder (Projektbeteiligte) umzusetzen. 

Erstellen Sie als erstes ein auf den Kunden zugeschnittenes IT-Konzept **(3. Teilauftrag)** und realisieren Sie dieses gem. den vorgängig definierten Standardvorgaben **(4. Teilauftrag)**

**Viel Spass und gutes Gelingen**

---
<br>

**Vorgehensweise**

Dieses Projekt ist in **vier** Abschnitte unterteilt. Jeder der vier Teilaufträge entspricht einem separaten Leistungsnachweis, den Sie in der vorgegebenen Reihenfolge bearbeiten.  

- **1. Teilauftrag:** <br>
Planung der Gebäudeverkabelung für die HW-Installation und Design des logischen Netzwerkplanes.<br>
Details: [01_Physik+Log_Netzwerktopologie_v1.2.md](01_Physik+Log_Netzwerktopologie_v1.2.md)

- **2. Teilauftrag:** <br>
Material- und Dienstleistungskosten für die Realisierung. Erstellen einer Offerte (Kostenkalkulation für Material, Installation und Konfiguration der IT-Infrastruktur).<br>
Details: [02_Offerte_erstellen_v1.3.md](02_Offerte_erstellen_v1.3.md)

- **3. Teilauftrag:**<br>
IT-Konzept & Systemdokumentation (ICT-Komponenten nach Standardvorgaben und Konventionen bestimmen und dokumentieren).<br>
Downloaden Sie dazu das Dokument [03_IT-Konzept_+_Systemdoku_v1.2.pdf](03_IT-Konzept_+_Systemdoku_v1.2.pdf) runter und füllen Sie die Textfelder im PDF aus. Damit der Inhalt nicht verloren geht, dürfen Sie nicht vergessen, dieses zu sichern.

- **4. Teilauftrag:**<br>
Umsetzung (Hands-on Lab). Der Coach teilt Ihnen "random" **zwei Benutzer** der Firma Sota GmbH zu. 
Installieren und konfigurieren Sie die entsprechenden zwei Arbeitsstationen (Anwender-PC mit Win10. Professional auf zwei virtuellen Maschinen) unter Einhaltung der vorgängig definierten Standardvorgaben (TA3: IT-Konzept & Systemdokumentation).<br>
Details: [04_Installation_+_Konfiguration_v1.2.md](04_Installation_+_Konfiguration_v1.2.md)
---
<br>

**Tipps**

- Die Ausgangslage oben sollte **absolut klar** sein. Nehmen Sie sich Zeit dafür. Andernfalls fragen sie nach. Es lohnt sich später.

- Achten Sie auf die **Reihenfolge** der Teilaufträge. Diese sind der Reihe nach zu bearbeiten. Starten Sie mit dem **ersten Teilauftrag**.

- Falls Sie in Teams arbeiten: Definieren Sie, **wer für was verantwortlich** ist

- Suchen Sie eigene, kreative Lösungen
