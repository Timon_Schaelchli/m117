# Vorschlag für Durchführung und Gewichtung der LBs

**LB1 Schriftliche Lernkontrolle**<br>
Die **erste Leistungsbeurteilung**. Schriftliche Lernkontrolle. Findet ca. 3-4 Wochen nach dem Modulstart statt.<br>
**Modulgewichtung: 40%**

<br>

**LB2 Projektarbeit (Sozialform vorzugsweise Tandem)**<br>
Bei der **zweiten Leistungsbeurteilung** handelt es sich um ein SOL-Projekt mit **vier Teilaufträgen**, welches direkt im Anschluss an die LB1 gestartet werden kann. Um den Lernenden genügend Zeit zur Verfügung zu stellen, sollten ca. **20 Lektionen** dafür eingerechnet werden.
Jeder Teilauftrag wird vorzugsweise separat abgegeben (Terminvereinbarung mit der Lehrperson).<br>
**Modulgewichtung: 50%** 

Die Aufträge der **LB2** sind wie folgt gegliedert:

- [Gesamtauftrag](LB2/00_Auftrag_IT-Konzept_1.2.md)
- [Aufbau Physikalische und logische Netzwerktopologie](LB2/01_Physik+Log_Netzwerktopologie_v1.2.md) **(15% Gewichtung)**
- [Erstellen einer Offerte (inkl. Material und DL-Kosten)](LB2/02_Offerte_erstellen_v1.2.md) **(15% Gewichtung)**
- [Erstellen eines IT-Konzepts](LB2/03_IT-Konzept_+_Systemdoku_v1.2.pdf) **(15% Gewichtung)**
- [Installation + Konfiguration von zwei Arbeitsplätzen](LB2/03_IT-Konzept_+_Systemdoku_v1.2.pdf) **(55% Gewichtung)**

[Hier gehts zur **LB2**](LB2)

<br>

Die restliche Unterrichtszeit kann für weitere Themen (siehe Kompetenzmatrix) genutzt werden.

<br>

Bei Fragen, Anpassungen oder Wünschen, kannst Du Dich sehr gerne bei mir melden.

Modulverantwortung:
[Marcello Calisto](mailto://marcello.calisto@tbz.ch) 
